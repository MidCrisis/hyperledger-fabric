/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Context, Contract, Info, Returns, Transaction } from 'fabric-contract-api';
import { MyAssetTwo } from './my-asset-two';

@Info({title: 'MyAssetTwoContract', description: 'My Smart Contract' })
export class MyAssetTwoContract extends Contract {

    @Transaction(false)
    @Returns('boolean')
    public async myAssetTwoExists(ctx: Context, myAssetTwoId: string): Promise<boolean> {
        const buffer = await ctx.stub.getState(myAssetTwoId);
        return (!!buffer && buffer.length > 0);
    }

    @Transaction()
    public async createMyAssetTwo(ctx: Context, myAssetTwoId: string, value: string): Promise<void> {
        const exists = await this.myAssetTwoExists(ctx, myAssetTwoId);
        if (exists) {
            throw new Error(`The my asset two ${myAssetTwoId} already exists`);
        }
        const myAssetTwo = new MyAssetTwo();
        myAssetTwo.value = value;
        const buffer = Buffer.from(JSON.stringify(myAssetTwo));
        await ctx.stub.putState(myAssetTwoId, buffer);
    }

    @Transaction(false)
    @Returns('MyAssetTwo')
    public async readMyAssetTwo(ctx: Context, myAssetTwoId: string): Promise<MyAssetTwo> {
        const exists = await this.myAssetTwoExists(ctx, myAssetTwoId);
        if (!exists) {
            throw new Error(`The my asset two ${myAssetTwoId} does not exist`);
        }
        const buffer = await ctx.stub.getState(myAssetTwoId);
        const myAssetTwo = JSON.parse(buffer.toString()) as MyAssetTwo;
        return myAssetTwo;
    }

    @Transaction()
    public async updateMyAssetTwo(ctx: Context, myAssetTwoId: string, newValue: string): Promise<void> {
        const exists = await this.myAssetTwoExists(ctx, myAssetTwoId);
        if (!exists) {
            throw new Error(`The my asset two ${myAssetTwoId} does not exist`);
        }
        const myAssetTwo = new MyAssetTwo();
        myAssetTwo.value = newValue;
        const buffer = Buffer.from(JSON.stringify(myAssetTwo));
        await ctx.stub.putState(myAssetTwoId, buffer);
    }

    @Transaction()
    public async deleteMyAssetTwo(ctx: Context, myAssetTwoId: string): Promise<void> {
        const exists = await this.myAssetTwoExists(ctx, myAssetTwoId);
        if (!exists) {
            throw new Error(`The my asset two ${myAssetTwoId} does not exist`);
        }
        await ctx.stub.deleteState(myAssetTwoId);
    }

}
