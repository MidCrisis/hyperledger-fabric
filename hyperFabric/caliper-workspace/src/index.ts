/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { MyAssetTwoContract } from './my-asset-two-contract';
export { MyAssetTwoContract } from './my-asset-two-contract';

export const contracts: any[] = [ MyAssetTwoContract ];
