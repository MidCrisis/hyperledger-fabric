/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Context } from 'fabric-contract-api';
import { ChaincodeStub, ClientIdentity } from 'fabric-shim';
import { MyAssetTwoContract } from '.';

import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import * as sinonChai from 'sinon-chai';
import winston = require('winston');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext implements Context {
    public stub: sinon.SinonStubbedInstance<ChaincodeStub> = sinon.createStubInstance(ChaincodeStub);
    public clientIdentity: sinon.SinonStubbedInstance<ClientIdentity> = sinon.createStubInstance(ClientIdentity);
    public logging = {
        getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
        setLevel: sinon.stub(),
     };
}

describe('MyAssetTwoContract', () => {

    let contract: MyAssetTwoContract;
    let ctx: TestContext;

    beforeEach(() => {
        contract = new MyAssetTwoContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"my asset two 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"my asset two 1002 value"}'));
    });

    describe('#myAssetTwoExists', () => {

        it('should return true for a my asset two', async () => {
            await contract.myAssetTwoExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a my asset two that does not exist', async () => {
            await contract.myAssetTwoExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createMyAssetTwo', () => {

        it('should create a my asset two', async () => {
            await contract.createMyAssetTwo(ctx, '1003', 'my asset two 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"my asset two 1003 value"}'));
        });

        it('should throw an error for a my asset two that already exists', async () => {
            await contract.createMyAssetTwo(ctx, '1001', 'myvalue').should.be.rejectedWith(/The my asset two 1001 already exists/);
        });

    });

    describe('#readMyAssetTwo', () => {

        it('should return a my asset two', async () => {
            await contract.readMyAssetTwo(ctx, '1001').should.eventually.deep.equal({ value: 'my asset two 1001 value' });
        });

        it('should throw an error for a my asset two that does not exist', async () => {
            await contract.readMyAssetTwo(ctx, '1003').should.be.rejectedWith(/The my asset two 1003 does not exist/);
        });

    });

    describe('#updateMyAssetTwo', () => {

        it('should update a my asset two', async () => {
            await contract.updateMyAssetTwo(ctx, '1001', 'my asset two 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"my asset two 1001 new value"}'));
        });

        it('should throw an error for a my asset two that does not exist', async () => {
            await contract.updateMyAssetTwo(ctx, '1003', 'my asset two 1003 new value').should.be.rejectedWith(/The my asset two 1003 does not exist/);
        });

    });

    describe('#deleteMyAssetTwo', () => {

        it('should delete a my asset two', async () => {
            await contract.deleteMyAssetTwo(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a my asset two that does not exist', async () => {
            await contract.deleteMyAssetTwo(ctx, '1003').should.be.rejectedWith(/The my asset two 1003 does not exist/);
        });

    });

});
